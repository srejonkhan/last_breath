﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class automaticNotifications : MonoBehaviour
{

	public Text titleText;
	public Text messageText;
	public GameObject notificationPlate;
	public Animation anims;
	public AnimationClip pop_Up;
	public AnimationClip pop_Out;

	public static automaticNotifications instance;

	public void Awake()
	{
		instance = this;
		notificationPlate.SetActive (true);
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.I))
			displayNotification ("Yahoo!!!", "I have made the UI\nNothing is pleasent than completing a work!", 3f);
	}

	public void displayNotification(string title,string message, float duration)
	{
		anims.CrossFade (pop_Up.name);
		titleText.text = title ;
		messageText.text = message;

		notificationPlate.SetActive (true);
		duration = Mathf.Clamp (duration, pop_Up.length, float.MaxValue);
		StartCoroutine (Stop(duration));
	}
	IEnumerator Stop( float duration)
		{

		yield return new WaitForSeconds (duration);
		anims.CrossFade (pop_Out.name);
		StartCoroutine (End());

	}

		IEnumerator End()
		{
		yield return new WaitForSeconds (pop_Out.length);
		notificationPlate.SetActive (true);
		}

}
