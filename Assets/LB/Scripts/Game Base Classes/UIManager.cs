﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Weapon")]
    [Space]
    public Text CurrentBullet;
    public Text BulletLeft;
    public Text WeaponName;
    [Space(5)]

    [Header("Event Notification")]
    [Space]
    public Animation AnimEN;
    public AnimationClip en_PopOutAnimation;
    public AnimationClip en_PopInAnimation;
    [Space]
    public Text en_TitleText;
    public Text en_MessageText;
    public GameObject en_Holder;
    [Space(5)]

    [Header("Info Notification")]
    [Space]
    public Animation AnimIN;
    public AnimationClip in_PopOutAnimation;
    public AnimationClip in_PopInAnimation;
    [Space]
    public Text in_MessageText;
    public GameObject in_Holder;
    [Space(5)]

    [Header("Pickup")]
    [Space]
    public GameObject PickupText;
    public GameObject PickupTextNegative;

    public static UIManager instance;

    void Awake()
    {
        instance = this;
    }
    void Start()
    {

    }
    void Update()
    {

    }
    /// <summary>
    /// For Display Event Notification
    /// </summary>
    /// <param Header Title="title"></param>
    /// <param Main Message="message"></param>
    /// <param Duration of Notification="duration"></param>
    /// <returns></returns>
    public IEnumerator DisplayEN(string title, string message, float duration)
    {
        en_Holder.SetActive(true);

        AnimEN.CrossFade(en_PopOutAnimation.name);
        en_TitleText.text = title;
        en_MessageText.text = message;

        en_Holder.SetActive(true);
        duration = Mathf.Clamp(duration, en_PopOutAnimation.length, float.MaxValue);

        yield return new WaitForSeconds(duration);

        AnimEN.CrossFade(en_PopInAnimation.name);

        yield return new WaitForSeconds(en_PopInAnimation.length);
        en_Holder.SetActive(false);
    }

    /// <summary>
    /// For Display Info Notification (Bottom)
    /// </summary>
    /// <param Main Message that will show="message"></param>
    /// <param Timestamp of Notification="duration"></param>
    /// <returns></returns>
    public IEnumerator DisplayIN(string message, float duration)
    {
        in_Holder.SetActive(true);

        AnimEN.CrossFade(in_PopOutAnimation.name);
        in_MessageText.text = message;

        in_Holder.SetActive(true);
        duration = Mathf.Clamp(duration, in_PopOutAnimation.length, float.MaxValue);

        yield return new WaitForSeconds(duration);

        AnimEN.CrossFade(in_PopInAnimation.name);

        yield return new WaitForSeconds(in_PopInAnimation.length);
        in_Holder.SetActive(false);
    }

    /// <summary>
    /// Enable when Ray hit Pickupable Object
    /// </summary>
    /// <param Input Button ="ButtonName"></param>
    /// <param Pickupable Object Name="PickupableName"></param>
    public void DisplayPickup(string ButtonName, string PickupableName)
    {
        PickupText.SetActive(true);
        PickupText.GetComponent<Text>().text = "Press <b><color=#FF6F00FF>[" + ButtonName + "]</color></b> to Pickup <b><color=#FF6F00FF>" + PickupableName + "</color></b>";
    }

    public void DisplayNegetivePickup()
    {
        PickupText.SetActive(true);
        PickupText.GetComponent<Text>().text = "You already Have this <b><color=#FF6F00FF>Weapon</color></b>";
    }

    public void PickupHideAll()
    {
        PickupText.SetActive(false);
        PickupTextNegative.SetActive(false);

    }
}
