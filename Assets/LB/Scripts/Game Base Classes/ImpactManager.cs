﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactManager : MonoBehaviour
{
    public GameObject GenericImpact;
    [Space]
    public GameObject DirtImpact;
    [Space]
    public GameObject SandImpact;
    [Space]
    public GameObject ConcreteImpact;
    [Space]
    public GameObject WoodImpact;
    [Space]
    public GameObject BloodImpact;
    [Space]
    public GameObject MetalImpact;

    public static ImpactManager instance;

    void start()
    {
        instance = this;
    }
    
    void Update()
    {

    }
    public void Generic(RaycastHit hit, float force, bool addForce, float particeDestroyTime, float impactDestroyTime)
    {
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, hit.normal);

        GameObject ImpactHoleEffect = Instantiate(GenericImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
        ImpactHoleEffect.transform.localPosition += 0.02f * hit.normal;
      //  ImpactHoleEffect.transform.parent = hit.transform;
        Destroy(ImpactHoleEffect, impactDestroyTime);

        if (addForce)
        {
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * force);
            }
        }
    }

    public void Dirt(RaycastHit hit, float force, bool addForce, float particeDestroyTime, float impactDestroyTime)
    {
        GameObject ImpactHoleEffect = Instantiate(DirtImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
        ImpactHoleEffect.transform.localPosition += 0.02f * hit.normal;
      //  ImpactHoleEffect.transform.parent = hit.transform;
        Destroy(ImpactHoleEffect, impactDestroyTime);

        if (addForce)
        {
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * force);
            }
        }
    }

    public void Sand(RaycastHit hit, float force, bool addForce, float particeDestroyTime, float impactDestroyTime)
    {
        GameObject ImpactHoleEffect = Instantiate(SandImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
        ImpactHoleEffect.transform.localPosition += 0.02f * hit.normal;
     //   ImpactHoleEffect.transform.parent = hit.transform;
        Destroy(ImpactHoleEffect, impactDestroyTime);

        if (addForce)
        {
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * force);
            }
        }
    }

    public void Concrete(RaycastHit hit, float force, bool addForce, float particeDestroyTime, float impactDestroyTime)
    {
        GameObject ImpactHoleEffect = Instantiate(ConcreteImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
        ImpactHoleEffect.transform.localPosition += 0.02f * hit.normal;
    //    ImpactHoleEffect.transform.parent = hit.transform;
        Destroy(ImpactHoleEffect, impactDestroyTime);

        if (addForce)
        {
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * force);
            }
        }
    }

    public void Wood(RaycastHit hit, float force, bool addForce, float particeDestroyTime, float impactDestroyTime)
    {
        GameObject ImpactHoleEffect = Instantiate(WoodImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
        ImpactHoleEffect.transform.localPosition += 0.02f * hit.normal;
      //  ImpactHoleEffect.transform.parent = hit.transform;
        Destroy(ImpactHoleEffect, impactDestroyTime);

        if (addForce)
        {
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * force);
            }
        }
    }

    public void Blood(RaycastHit hit, float force, bool addForce, float particeDestroyTime, float impactDestroyTime)
    {
        GameObject ImpactHoleEffect = Instantiate(BloodImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
        ImpactHoleEffect.transform.localPosition += 0.02f * hit.normal;
     //   ImpactHoleEffect.transform.parent = hit.transform;
        Destroy(ImpactHoleEffect, impactDestroyTime);

        if (addForce)
        {
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * force);
            }
        }
    }

    public void Metal(RaycastHit hit, float force, bool addForce, float particeDestroyTime, float impactDestroyTime)
    {
        GameObject ImpactHoleEffect = Instantiate(MetalImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
        Destroy(ImpactHoleEffect, impactDestroyTime);

        if (addForce)
        {
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * force);
            }
        }
    }

}
