﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace ElectronicBrain.Other
{
    public class GameStartStuff : MonoBehaviour
    {
        public GameObject Player;
        // Use this for initialization
        void Start()
        {

            Player.GetComponent<FirstPersonController>().m_CanRun = true;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
