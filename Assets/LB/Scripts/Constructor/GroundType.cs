﻿using UnityEngine;

namespace ElectronicBrain.Constructor
{
    [System.Serializable]
    public class GroundType
    {
        public string name;
        [Space]
        public AudioClip[] footstepsounds;
        [Space]
        public float walkSpeed = 5;
        public float runSpeed = 10;
        [Space]
        public bool canRun;
    }
}
