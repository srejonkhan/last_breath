﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIBlinking : MonoBehaviour
{
    [Range(0,20)]public float SmoothingTime;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<CanvasGroup>().alpha = Mathf.Sin(Time.time * SmoothingTime);
	}

}
