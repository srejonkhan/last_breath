﻿using System.Collections;
using System.Collections.Generic;
using ElectronicBrain.Weapon;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class CrosshairManager : MonoBehaviour
{
    public RectTransform[] crosshairs;
    public GameObject Circle;
    public float walkSize;
    public float chSize;
    public static CrosshairManager instance;

    // Use this for initialization
    void Start()
    {
        instance = this;
        walkSize = crosshairs[0].localPosition.y;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCrosshair();
    }
    public void UpdateCrosshair()
    {
        float crossHairSize = CalculateCrossHair();

        crosshairs[0].localPosition = Vector3.Slerp(crosshairs[0].localPosition, new Vector3(0f, crossHairSize, 0f), Time.deltaTime * 8f);
        crosshairs[1].localPosition = Vector3.Slerp(crosshairs[1].localPosition, new Vector3(crossHairSize, 0f, 0f), Time.deltaTime * 8f);
        crosshairs[2].localPosition = Vector3.Slerp(crosshairs[2].localPosition, new Vector3(-crossHairSize, 0f, 0f), Time.deltaTime * 8f);
        crosshairs[3].localPosition = Vector3.Slerp(crosshairs[3].localPosition, new Vector3(0f, -crossHairSize, 0f), Time.deltaTime * 8f);

    }


    public float CalculateCrossHair()
    {
        if (WeaponManager.instance.Weapon_In_Inventory[WeaponManager.instance.index].gameObject.GetComponent<WeaponIndex>().Index != 0)
        {
            if (WeaponManager.instance.Weapon_In_Inventory[WeaponManager.instance.index].GetComponent<WeaponIndex>().Index != 4)
            {
                chSize = WeaponManager.instance.Weapon_In_Inventory[WeaponManager.instance.index].gameObject.GetComponent<WeaponBase>().crossHairSize;
            }
            if (WeaponManager.instance.Weapon_In_Inventory[WeaponManager.instance.index].GetComponent<WeaponIndex>().Index == 4)
            {
               // chSize = Shotgun.Instance.crossHairSize;
            }
        }

        float size = walkSize * chSize;

        if (FirstPersonController.instance.isRunning)
        {
            if (FirstPersonController.instance.gameObject.GetComponent<ElectronicBrain.Player.StaminaSystem>().Stamina >= 1)
            {
                size *= 1.5f;
            }
        }

        if (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.D)))))
        {
            if (FirstPersonController.instance.m_IsWalking)
            {
                size *= 1;
            }
        }
        else
            size /= 1.5f;

        return size;
    }
}
