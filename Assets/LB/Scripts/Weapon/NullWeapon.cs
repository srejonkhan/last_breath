﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NullWeapon : MonoBehaviour
{

    void Awake()
    {
        //
        Invoke("Functionality", 1f);
    }

    void Functionality()
    {
        VisibleLeg.instance.hasRifle = false;
        UIManager.instance.CurrentBullet.text = "--";
        UIManager.instance.BulletLeft.text = "/--";
        UIManager.instance.WeaponName.text = "---";
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
