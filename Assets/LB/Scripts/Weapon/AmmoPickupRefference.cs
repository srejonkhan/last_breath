﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ElectronicBrain.Weapon
{
    public class AmmoPickupRefference : MonoBehaviour
    {

        public static AmmoPickupRefference instance;
        public int Index_;
        public int _Ammo;

        void Start()
        {
            instance = this;
        }
    }
}
