﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponIndex : MonoBehaviour
{
    public string Name;
    public int Index;
    public static WeaponIndex instance;

    // Use this for initialization
    void Start()
    {
        instance = this;
    }
}
