﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSway : MonoBehaviour
{

    public float Ammount;
    public float Smoothness;
    public float MaxAmount;

    public Vector3 InitialPosition;

    // Use this for initialization
    void Start()
    {
        InitialPosition = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {

        float movementX = -Input.GetAxis("Mouse X") * Ammount;
        float movementY = -Input.GetAxis("Mouse Y") * Ammount;

        movementX = Mathf.Clamp(movementX, -MaxAmount, MaxAmount);
        movementY = Mathf.Clamp(movementY, -MaxAmount, MaxAmount);

        Vector3 finalPos = new Vector3(movementX, movementY, 0);
        transform.localPosition = Vector3.Lerp(transform.localPosition, finalPos + InitialPosition, Time.deltaTime * Smoothness);
    }
}
