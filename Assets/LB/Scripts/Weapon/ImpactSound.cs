﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ElectronicBrain.Other
{
    public class ImpactSound : MonoBehaviour
    {

        void Awake()
        {
            GetComponent<AudioSource>().Play();

        }

    }
}
