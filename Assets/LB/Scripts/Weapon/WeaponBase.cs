﻿using System.Collections;
using System.Collections.Generic;
using ElectronicBrain.Player;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace ElectronicBrain.Weapon
{
    public class WeaponBase : MonoBehaviour
    {

        public enum FireType { Auto, Semi, Sniper, Shotgun };
        public FireType FiringType;
        [Space]

        [Range(0, 1)] public float FireRate;
        [Space(5)]

        [Header("•••FIRE ATTRIBUTES•••")]
        [Space]
        public string WeaponName;
        [Space]
        public bool CanShoot = true;
        [Space]
        public float Range;
        public int Damage;
        public float crossHairSize;
        public float HitForce = 100f;
        public float Recoil = 0.001f;
        public Camera RaycastCamera;
        float timePassed = 0f;
        float fireTimer;
        [Space(5)]

        [Header("•••DETAILS ATTRIBUTES•••")]
        [Space]
        public bool shootProjectile;
        [Space]
        public GameObject Shell;
        public GameObject Bullet;
        public Transform ShellEject;
        [Space]
        public GameObject MuzzleFlash;
        [Space]
        [Header("Kickback")]
        public Transform kickGO;
        public float kickUp = 0.5f;
        public float kickSideways = 0.5f;
        [Space(5)]

        [Header("•••FIRE-ARM ATTRIBUTES•••")]
        [Space]
        public int BulletsPerMag = 30;
        public int BulletsLeftOnMag = 200;
        public int CurrentBullet;
        [Space(5)]

        [Header("•••SOUND ATTRIBUTES•••")]
        [Space]
        public AudioClip DrawSound;
        public AudioClip ShootSound;
        public AudioClip ReloadSound;
        private AudioSource audioSource;


        private Animator Anim;
        private bool FireInput;//Get Player Input
        private bool isReloading;

        public GameObject GameManager;

        void Awake()
        {
             Invoke("InstantiateJob",0.1f);
        }

        void InstantiateJob()
        {
            audioSource.PlayOneShot(DrawSound);
            VisibleLeg.instance.hasRifle = true;
        }

        void Start()
        {
            //GetReference - START 
            Anim = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
          //  GameManager = GameObject.Find("GameManager");
            //GetReference - END
        }

        void Update()
        {
            timePassed += Time.deltaTime;

            switch (FiringType)
            {
                case FireType.Auto:
                    FireInput = Input.GetButton("Fire1");
                    break;
                case FireType.Semi:
                    FireInput = Input.GetButtonDown("Fire1");
                    break;
            }

            UI();
            Funtionality();
        }

        void UI()
        {
            UIManager.instance.CurrentBullet.text = CurrentBullet.ToString();
            UIManager.instance.BulletLeft.text = "/"+ BulletsLeftOnMag.ToString();
            UIManager.instance.WeaponName.text = WeaponName.ToString();
        }

        void FixedUpdate()
        {
            AnimatorStateInfo animInfo = Anim.GetCurrentAnimatorStateInfo(0);
            isReloading = animInfo.IsName("Reload");
        }

        void Funtionality()
        {
            //ShootFunctionality-START
            if ((FireInput) && CanShoot)
            {
                if (!isReloading)
                {
                    Debug.Log("Input");
                    if (CurrentBullet > 0)
                        Fire();
                    else if (BulletsLeftOnMag > 0)
                        DoReload();
                }
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                if (CurrentBullet < BulletsPerMag && BulletsLeftOnMag > 0)
                    DoReload();
            }

            //Change Weapon Rotation While Running - START
            if (FirstPersonController.instance.isRunning)
            {
                if (FirstPersonController.instance.gameObject.GetComponent<StaminaSystem>().Stamina >= 1)
                {
                    Run();
                }
            }
            if (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.D)))))
            {
                if (!FirstPersonController.instance.isRunning)
                {
                    Walk();
                }
            }
            else
            {
                Anim.SetBool("Run", false);
                Anim.SetBool("Walk", false);
            }
            if (isReloading)
            {
                //Anim.SetBool("Reload", true);
                WeaponManager.instance.CanInput = false;
            }
            if (fireTimer < FireRate)
                fireTimer += Time.deltaTime;
        }

        void Fire()
        {
            //AI GunShot Event and Weapon Manager Event
            Debug.Log("Fire");
            if (fireTimer < FireRate) return;

            WeaponManager.instance.CanInput = false;
            if (fireTimer < FireRate) return;

            //CrosshairManager.instance.chSize += 0.1f;

            RaycastHit Hit;
            Ray ray = RaycastCamera.ScreenPointToRay(new Vector3(Screen.width / 2 + Random.Range(-CrosshairManager.instance.CalculateCrossHair() / 1.4f, CrosshairManager.instance.CalculateCrossHair() / 1.4f), Screen.height / 2 + Random.Range(-CrosshairManager.instance.CalculateCrossHair() / 1.4f, CrosshairManager.instance.CalculateCrossHair() / 1.4f), 0));


            if (Physics.Raycast(ray, out Hit, Range))
            {
                //Debug.Log (Hit.transform.name + "Has Been Hitted");
                if (Hit.rigidbody != null)
                {
                    GameManager.GetComponent<ImpactManager>().Generic(Hit,HitForce, true, 2f, 2f);
                }

                if (Hit.transform.tag == "ZombieAI")
                {
                    GameManager.GetComponent<ImpactManager>().Blood(Hit, HitForce, true, 2f, 2f);
                }

                if (Hit.transform.tag == "Concrete")
                {
                    GameManager.GetComponent<ImpactManager>().Concrete(Hit, HitForce, true, 2f, 2f);
                }

                if (Hit.transform.tag == "Blood")
                {
                    GameManager.GetComponent<ImpactManager>().Blood(Hit, HitForce, true, 2f, 2f);
                }

                if (Hit.transform.tag == "Sand")
                {
                    GameManager.GetComponent<ImpactManager>().Sand(Hit, HitForce, true, 2f, 2f);
                }

                if (Hit.transform.tag == "Metal")
                {
                    GameManager.GetComponent<ImpactManager>().Metal(Hit, HitForce, true, 2f, 2f);
                }

                if (Hit.transform.tag == "Wood")
                {
                    GameManager.GetComponent<ImpactManager>().Wood(Hit, HitForce, true, 2f, 2f);
                }
            }

            Anim.CrossFadeInFixedTime("Fire", 0.1f);

            MuzzleFlash.SetActive(false);
            MuzzleFlash.SetActive(true);

            kickGO.localRotation = Quaternion.Euler(kickGO.localRotation.eulerAngles - new Vector3(kickUp, Random.Range(-kickSideways, kickSideways), 0));

            audioSource.PlayOneShot(ShootSound);

            if (shootProjectile)
            {
                GameObject _Shell = GameObject.Instantiate(Shell, ShellEject.position, Quaternion.identity);
                Destroy(_Shell, 3f);
            }
            CurrentBullet--;

            fireTimer = 0.0f;
            timePassed = 0f;

           // CrosshairManager.instance.chSize -= 0.1f;

            WeaponManager.instance.CanInput = true;

        }

        void DoReload()
        {
            if (BulletsLeftOnMag <= 0)
                return;
            AnimatorStateInfo info = Anim.GetCurrentAnimatorStateInfo(0);
            if (isReloading)
                return;
            Anim.CrossFadeInFixedTime("Reload", 0.01f);
            int BulletToLoad = BulletsPerMag - CurrentBullet;
            int BulletToSubst = (BulletsLeftOnMag >= BulletToLoad) ? BulletToLoad : BulletsLeftOnMag;// if BulletLeftOnMag is equal Great than BulletToLoad THEN first BulletToLoad or Second Bullet Left on Mag!!
            BulletsLeftOnMag -= BulletToSubst;
            CurrentBullet += BulletToSubst;
            audioSource.PlayOneShot(ReloadSound);

        }

        void Run()
        {
            Anim.SetBool("Run", true);
            Anim.SetBool("Walk", false);
        }

        void Walk()
        {
            Debug.Log("Walk Anim");
            Anim.SetBool("Walk", true);
            Anim.SetBool("Run", false);
        }
    }
}
