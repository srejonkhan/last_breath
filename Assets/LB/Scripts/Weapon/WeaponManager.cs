﻿using System.Collections;
using System.Collections.Generic;
using ElectronicBrain.Weapon;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public GameObject GameManager;
    [Space]
    [Header("Weapon in Player Inventory")]
    [Space]

    public GameObject[] Weapon_In_Inventory;
    [Space]

    [Header("Weapon in Whole Games")]
    public GameObject[] Weapon_In_Games;
    [Space(5)]

    [Header("Weapon PickUP prefabs in Games")]
    public Rigidbody[] Weapon_Pickup;
    [Space(5)]

    [Header("Weapon Camera for RayCast")]
    [Space]
    public Transform DropPos;

    [Header("Weapon Camera for RayCast")]
    [Space]
    public Camera Weapon_Camera;

    [Space(5)]
    [Header("•••Electronic Brain•••")]

    public bool CanInput = true;
    public int index = 0;

    bool isEquiped;
    int WeaponToSet;
    int WeaponToDrop;
    bool canDrop = true;

    
    public static WeaponManager instance;

    // Use this for initialization
    void Start()
    {
        instance = this;
        Weapon_In_Inventory[index].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        _Input();
        _Raycast();
        CH_Fix();// active cirle while using melee weapon or null

    }
    void _Input()
    {

        if (CanInput)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                StartCoroutine(Delay());
                index--;
                if (index < 0)
                {
                    index = Weapon_In_Inventory.Length - 1;
                }

                StartCoroutine(DeactiveWeapon());
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                StartCoroutine(Delay());
                index++;
                if (index > (Weapon_In_Inventory.Length - 1))
                {
                    index = 0;
                }
                StartCoroutine(DeactiveWeapon());
            }
            if (canDrop)
            {
                if (Input.GetKeyDown(KeyCode.G))
                {
                    //    StartCoroutine(Delay());
                    StartCoroutine(DropWeapon_Inventory());
                }
            }
        }
    }
    void _Raycast()
    {
        Ray ray = Weapon_Camera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 10f))
        {
            if (hit.transform.gameObject.CompareTag("Ammo"))
            {
                if (hit.transform.gameObject.GetComponent<AmmoPickupRefference>() != null)
                {
                    if (hit.transform.gameObject.GetComponent<AmmoPickupRefference>().Index_ == index)
                    {
                        GameManager.GetComponent<UIManager>().DisplayPickup("E", "Ammo");
                        if (Input.GetKeyDown(KeyCode.E))
                        {
                            Weapon_In_Inventory[index].GetComponent<WeaponBase>().BulletsLeftOnMag += hit.transform.gameObject.GetComponent<AmmoPickupRefference>()._Ammo;
                            Destroy(hit.transform.gameObject);
                            UIManager.instance.PickupHideAll();
                        }
                    }
                    if (hit.transform.gameObject.GetComponent<AmmoPickupRefference>().Index_ != index)
                    {
                        GameManager.GetComponent<UIManager>().DisplayNegetivePickup();
                    }
                }

            }
            if (hit.transform.gameObject.CompareTag("WeaponPickup"))
            {
                Debug.Log("WeaponPick");
                if (hit.transform.gameObject.GetComponent<WeaponIndex>() != null)
                {
                    WeaponToSet = hit.transform.gameObject.GetComponent<WeaponIndex>().Index;
                    if (Weapon_In_Inventory[0] != Weapon_In_Games[WeaponToSet] && Weapon_In_Inventory[1] != Weapon_In_Games[WeaponToSet] && Weapon_In_Inventory[2] != Weapon_In_Games[WeaponToSet])
                    {
                        //UIManager.instance.DisplayPickup("E", Weapon_In_Inventory[index].GetComponent<WeaponBase>().WeaponName);
                        GameManager.GetComponent<UIManager>().DisplayPickup("E", hit.transform.gameObject.GetComponent<WeaponIndex>().Name);
                        isEquiped = false;
                    }
                    else
                    {
                        Debug.Log("Wrong WeaponPick");
                        GameManager.GetComponent<UIManager>().PickupHideAll();
                        GameManager.GetComponent<UIManager>().DisplayNegetivePickup();
                        isEquiped = true;
                    }
                    if (!isEquiped)
                    {
                        if (Input.GetKeyDown(KeyCode.E))
                        {

                            PickupWeapon();
                            Destroy(hit.transform.gameObject);
                        }
                    }
                }
            }
            else
            {
                GameManager.GetComponent<UIManager>().PickupHideAll();
            }

        }
        else
        {
            GameManager.GetComponent<UIManager>().PickupHideAll();
        }
    }

    IEnumerator DropWeapon_Inventory()
    {
        //  StartCoroutine(Delay());
        canDrop = false;
        Weapon_In_Inventory[index].SetActive(false);

        int i = Weapon_In_Inventory[index].GetComponent<WeaponIndex>().Index;

        Rigidbody drop = Instantiate(Weapon_Pickup[i], DropPos.transform.position, DropPos.transform.rotation) as Rigidbody;
        drop.AddRelativeForce(0, 250, UnityEngine.Random.Range(100, 200));
        drop.AddTorque(-transform.up * 40);
        if (Weapon_In_Inventory[index] != Weapon_In_Games[0])
        {
            GameManager.GetComponent<UIManager>().DisplayIN("Dropped " + Weapon_Pickup[i].transform.GetComponent<WeaponIndex>().Name, 2f);
        }
        yield return new WaitForSeconds(1f);

        Weapon_In_Inventory[index] = Weapon_In_Games[0];
        ActivateWeapon(index);
        canDrop = true;

    }

    IEnumerator DeactiveWeapon()
    {
        foreach (GameObject go in Weapon_In_Inventory)
            go.SetActive(false);
        yield return new WaitForSeconds(1f);
        ActivateWeapon(index);
        yield return new WaitForSeconds(1f);
    }
    void ActivateWeapon(int i)
    {
        Weapon_In_Inventory[i].SetActive(true);
    }
    IEnumerator DropWeapon(int ID)
    {
        //  StartCoroutine(Delay());
        yield return new WaitForSeconds(0.3f);
        Rigidbody drop = Instantiate(Weapon_Pickup[ID], DropPos.transform.position, DropPos.transform.rotation) as Rigidbody;
        drop.AddRelativeForce(0, 250, UnityEngine.Random.Range(100, 200));
        drop.AddTorque(-transform.up * 40);
        yield return new WaitForSeconds(0.5f);
        Weapon_In_Inventory[index].SetActive(true);
    }
    void PickupWeapon()
    {
        if (index == 0)
        {
            foreach (GameObject go in Weapon_In_Inventory)
                go.SetActive(false);
            WeaponToDrop = Weapon_In_Inventory[0].gameObject.GetComponent<WeaponIndex>().Index;
            Weapon_In_Inventory[0] = Weapon_In_Games[WeaponToSet];
            StartCoroutine(DropWeapon(WeaponToDrop));
            // automaticNotifications.instance.displayNotification("Notification", "Picked Up Axe", 2f);
        }
        if (index == 1)
        {
            foreach (GameObject go in Weapon_In_Inventory)
                go.SetActive(false);
            WeaponToDrop = Weapon_In_Inventory[1].gameObject.GetComponent<WeaponIndex>().Index;
            Weapon_In_Inventory[1] = Weapon_In_Games[WeaponToSet];
            StartCoroutine(DropWeapon(WeaponToDrop));
            //automaticNotifications.instance.displayNotification("Notification", "Picked Up CZ-75", 2f);
        }
        if (index == 2)
        {
            foreach (GameObject go in Weapon_In_Inventory)
                go.SetActive(false);
            WeaponToDrop = Weapon_In_Inventory[2].gameObject.GetComponent<WeaponIndex>().Index;
            Weapon_In_Inventory[2] = Weapon_In_Games[WeaponToSet];
            StartCoroutine(DropWeapon(WeaponToDrop));
            //automaticNotifications.instance.displayNotification("Notification", "Picked Up AK-47", 2f);
        }
    }
    void CH_Fix()
    {
        //add if statement when you add  a new melee weapon
        /* 
           if(index = your new melee index)
           {
            foreach (RectTransform o in CrossHair_Manager.instance.crosshairs)
            {
                o.transform.gameObject.SetActive(false);
            }
            CrossHair_Manager.instance.Circle.SetActive(true);
           }*/


        if (Weapon_In_Inventory[index].GetComponent<WeaponIndex>().Index == 0)
        {
            foreach (RectTransform o in CrosshairManager.instance.crosshairs)
            {
                o.transform.gameObject.SetActive(false);
            }
            CrosshairManager.instance.Circle.SetActive(true);
        }
        if (Weapon_In_Inventory[index].GetComponent<WeaponIndex>().Index == 1)
        {
            foreach (RectTransform o in CrosshairManager.instance.crosshairs)
            {
                o.transform.gameObject.SetActive(false);
            }
            CrosshairManager.instance.Circle.SetActive(true);
        }
        if (Weapon_In_Inventory[index].GetComponent<WeaponIndex>().Index == 2)
        {
            foreach (RectTransform o in CrosshairManager.instance.crosshairs)
            {
                o.transform.gameObject.SetActive(true);
            }
            CrosshairManager.instance.Circle.SetActive(false);
        }
        if (Weapon_In_Inventory[index].GetComponent<WeaponIndex>().Index == 3)
        {
            foreach (RectTransform o in CrosshairManager.instance.crosshairs)
            {
                o.transform.gameObject.SetActive(true);
            }
            CrosshairManager.instance.Circle.SetActive(false);
        }
    }
    IEnumerator Delay()
    {
        CanInput = false;
        yield return new WaitForSeconds(2f);
        CanInput = true;
    }
}
