﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace ElectronicBrain.Other
{
    public class LadderAssistant : MonoBehaviour
    {
        void OnTriggerEnter(Collider col)
        {
            if (col.CompareTag("Player"))
            {
                col.GetComponent<FirstPersonController>().m_onLadder = true;
            }
        }
        void OnTriggerExit(Collider col)
        {
            if (col.CompareTag("Player"))
            {
                col.GetComponent<FirstPersonController>().m_onLadder = false;
            }
        }

    }
}
