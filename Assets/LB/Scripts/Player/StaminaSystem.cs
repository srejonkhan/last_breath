﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
namespace ElectronicBrain.Player
{
    public class StaminaSystem : MonoBehaviour
    {
        [Header("Stamina Attributes")]
        [Space(5)]
        public float Stamina;
        [Space]
        public float DecraseRate;
        public float IncreaseRate;
        [Space(5)]

        [Header("User Interface Attributes")]
        public GameObject RootUI;
        public Slider StaminaBar;
        [Space(5)]

        [Header("Audio Attributes")]
        public AudioClip BreathSound;
        public AudioSource BreathAudioSource;

        // Use this for initialization
        void Start()
        {
            BreathAudioSource.loop = true;
            BreathAudioSource.clip = BreathSound;
            BreathAudioSource.Play();
        }

        // Update is called once per frame
        void Update()
        {
            if (Stamina <= 0)
            {
                Stamina = 0; // Further Protection
                GetComponent<FirstPersonController>().m_CanRun = false;
            }
            if (Stamina >= 20 && FootstepSoundController.CanRun)
            {
                GetComponent<FirstPersonController>().m_CanRun = true;
            }

            Sound();
            IncreaseDecrease();
            UI();
            Fix();
        }
        void Sound()
        {
            if (Stamina <= 50)
            {
                BreathAudioSource.volume = Mathf.Lerp(BreathAudioSource.volume, (100 - Stamina) / 200f , Time.deltaTime);
            }

            /*
            if (Stamina <= 40)
            {
                BreathAudioSource.volume = Mathf.Lerp(BreathAudioSource.volume, 0.2f, Time.deltaTime);
            }
            if (Stamina <= 30)
            {
                BreathAudioSource.volume = Mathf.Lerp(BreathAudioSource.volume, 0.4f, Time.deltaTime);
            }
            if (Stamina <= 20)
            {
                BreathAudioSource.volume = Mathf.Lerp(BreathAudioSource.volume, 0.5f, Time.deltaTime);
            }
            if (Stamina <= 10)
            {
                BreathAudioSource.volume = Mathf.Lerp(BreathAudioSource.volume, 0.6f, Time.deltaTime);
            }*/
            else
            {
                BreathAudioSource.volume -= Time.deltaTime / 7;
            }
        }
        void IncreaseDecrease()
        {
            if (GetComponent<FirstPersonController>().isRunning && GetComponent<FirstPersonController>().m_CanRun)
            {
                Stamina -= DecraseRate * Time.deltaTime;
            }
            else
            {
                Stamina += IncreaseRate * Time.deltaTime;
            }
        }
        void UI()
        {
            StaminaBar.value = Stamina;

            if (GetComponent<FirstPersonController>().isRunning)
            {
                RootUI.GetComponent<CanvasGroup>().alpha += Time.deltaTime;
            }
            else
            {
                RootUI.GetComponent<CanvasGroup>().alpha -= Time.deltaTime / 2;
            }
        }

        void Fix()
        {
            if (Stamina >= 100)
            {
                Stamina = 100;
            }
        }
    }

}