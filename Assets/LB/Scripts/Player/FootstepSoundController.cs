﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using ElectronicBrain.Constructor;

namespace ElectronicBrain.Player
{
    public class FootstepSoundController : MonoBehaviour
    {
        public List<GroundType> GroundTypes = new List<GroundType>();
        FirstPersonController controller;
        public string CurrentGround;
        public static bool CanRun;
        [Space(5)]
        public string ConcreteTag = "Concrete";
        public string GrassTag = "Grass";
        public string WoodTag = "Wood";
        // Use this for initialization
        void Start()
        {
            controller = GetComponent<FirstPersonController>();
        }

        void OnControllerColliderHit(ControllerColliderHit col)
        {
            Debug.Log("OnControllerColliderHit");
            if (col.transform.tag == ConcreteTag)
            {
                setGroundType(GroundTypes[2]);
            }
            else if (col.transform.tag == GrassTag)
            {
                setGroundType(GroundTypes[1]);
            }
            else if (col.transform.tag == WoodTag)
            {
                setGroundType(GroundTypes[0]);
            }

        }

        public void setGroundType(GroundType ground)
        {
            Debug.Log("SetGroundType");
            if (CurrentGround != ground.name)
            {
                Debug.Log("SetGround If Statement");
                controller.m_FootstepSounds = ground.footstepsounds;
                controller.m_WalkSpeed = ground.walkSpeed;
                controller.m_RunSpeed = ground.runSpeed;
                CurrentGround = ground.name;
                if (!controller.m_onLadder)
                {
                    Debug.Log("Can Run Here");
                    controller.m_CanRun = ground.canRun;
                }
                else
                {
                    Debug.Log("Can't Run Here");
                    controller.m_CanRun = false;
                }
                CanRun = ground.canRun;
            }
            else
            {
                if (CurrentGround == ConcreteTag)
                {
                    controller.m_FootstepSounds = GroundTypes[2].footstepsounds;
                    controller.m_WalkSpeed = GroundTypes[2].walkSpeed;
                    controller.m_RunSpeed = GroundTypes[2].runSpeed;
                    CurrentGround = GroundTypes[2].name;
                    if (!controller.m_onLadder)
                    {
                        controller.m_CanRun = GroundTypes[2].canRun;
                    }
                    else
                    {
                        controller.m_CanRun = false;
                    }
                    CanRun = GroundTypes[2].canRun;
                }
                if (CurrentGround == GrassTag)
                {
                    controller.m_FootstepSounds = GroundTypes[1].footstepsounds;
                    controller.m_WalkSpeed = GroundTypes[1].walkSpeed;
                    controller.m_RunSpeed = GroundTypes[1].runSpeed;
                    CurrentGround = GroundTypes[1].name;
                    if (!controller.m_onLadder)
                    {
                        controller.m_CanRun = GroundTypes[1].canRun;
                    }
                    else
                    {
                        controller.m_CanRun = false;
                    }
                    CanRun = GroundTypes[1].canRun;
                }
                if (CurrentGround == WoodTag)
                {
                    controller.m_FootstepSounds = GroundTypes[0].footstepsounds;
                    controller.m_WalkSpeed = GroundTypes[0].walkSpeed;
                    controller.m_RunSpeed = GroundTypes[0].runSpeed;
                    CurrentGround = GroundTypes[0].name;
                    if (!controller.m_onLadder)
                    {
                        controller.m_CanRun = GroundTypes[0].canRun;
                    }
                    else
                    {
                        controller.m_CanRun = false;
                    }
                    CanRun = GroundTypes[0].canRun;
                }
            }
        }
    }
    
}
