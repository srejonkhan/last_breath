﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleLeg : MonoBehaviour
{
    public Animator BodyAnimation;
    [Space]
    public GameObject HandGO;

    [Space]
    public bool hasPistol;
    public bool hasRifle;
    [Space]
    public bool Idle;
    public bool Walk;
    public bool Run;
    public bool Jump;
    public bool RightStrafe;
    public bool LeftStrafe;
    [Space]
    public bool DoJump = false;

    public static VisibleLeg instance;

    void Awake()
    {
        instance = this;
    }
	// Use this for initialization
	void Start ()
    {
	
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S)) // Walk or Run
            {
                if (hasPistol || hasRifle)
                {
                   HandGO.SetActive(false);
                }

                if (!hasPistol && !hasRifle)
                {
                    HandGO.SetActive(true);
                }
                if (Input.GetKey(KeyCode.LeftShift))
                    BodyAnimation.SetInteger("Type_Free", 2);
                else
                    BodyAnimation.SetInteger("Type_Free", 1);

            }

            if (Input.GetKey(KeyCode.A)) // Left Strafe
            {
                if (hasPistol || hasRifle)
                {
                    HandGO.SetActive(false);
                }

                if (!hasPistol && !hasRifle)
                {
                    HandGO.SetActive(true);
                }

                BodyAnimation.SetInteger("Type_Free", 5);

            }

            if (Input.GetKey(KeyCode.D)) // Right Strafe
            {
                if (hasPistol || hasRifle)
                {
                    HandGO.SetActive(false);
                }

                if (!hasPistol && !hasRifle)
                {
                    HandGO.SetActive(true);
                }

                BodyAnimation.SetInteger("Type_Free", 4);
            }

            if (Input.GetKeyDown(KeyCode.Space) && !DoJump) // Jump
            {
                StartCoroutine(JumpAnimation());
            }

            if(DoJump)
            {
                if (hasPistol || hasRifle)
                {
                    HandGO.SetActive(false);
                }

                if (!hasPistol && !hasRifle)
                {
                    HandGO.SetActive(true);
                }

                BodyAnimation.SetInteger("Type_Free", 3);
            }
        }

        else
        {
            if (hasPistol || hasRifle)
            {
                HandGO.SetActive(false);
            }

            if (!hasPistol && !hasRifle)
            {
                HandGO.SetActive(true);
            }

            BodyAnimation.SetInteger("Type_Free", 0);
        }
	}

    IEnumerator JumpAnimation()
    {
        DoJump = true;
        yield return new WaitForSeconds(0.75f);
        DoJump = false;

    }
}
