﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BaseEditor : EditorWindow
{
    string Date = "DD/MM/YYYY";
    string Subject = "Theory";
    string Notes = "Note.....";

    [MenuItem("Electronic Brain/DevHelper/DevNote")]

    public static void ShowWindow()
    {
        GetWindow<BaseEditor>(false, "DevNote", true);
    }
    void Update()
    {

    }
    void OnGUI()
    {
        Date = EditorGUILayout.TextField("Date:", Date);
        Subject =  EditorGUILayout.TextField("Subject:", Subject);
        Notes = EditorGUILayout.TextArea( Notes , GUILayout.Height(position.height - 120));

        if (GUILayout.Button("GO"))
        {
            EditorPrefs.Date = Date;
            EditorPrefs.Subject = Subject;
            EditorPrefs.Note = Notes;
            GetWindow<DevNote>(false, "DevNoteWindow", true);
        }

    }


}
