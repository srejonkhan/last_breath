﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DevNote : EditorWindow
{
    [MenuItem("Electronic Brain/DevHelper/DevNoteWindow")]
    void OnGUI()
    {
        GUILayout.Label("Date: " + EditorPrefs.Date );
        GUILayout.Label("Subject: " + EditorPrefs.Subject );
        GUILayout.Label("Developer Note : \n \n" + EditorPrefs.Note , EditorStyles.boldLabel);
    }
}
