﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashlightOffset : MonoBehaviour
{
    private Vector3 offset;
    private GameObject GoFollow;
    public float Speed = 1.0f;

	// Use this for initialization
	void Start ()
    {
        GoFollow = Camera.main.gameObject;
        offset = transform.position - GoFollow.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = GoFollow.transform.position + offset;
        transform.rotation = Quaternion.Slerp(transform.rotation, GoFollow.transform.rotation, Speed * Time.deltaTime);
		
	}
}
