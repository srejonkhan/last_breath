﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashlightManager : MonoBehaviour
{

    [Header("Base Attributes")]
    [Space]
    public bool LiteFlashlight;
    public bool JumboFlashlight;
    [Space]
    public bool hasLF;
    public bool hasJF;
    [Space]
    public GameObject LF_Go;
    public GameObject JF_Go;
    [Space]
    public float Health;
    public float MaxHealth;
    [Space]
    public int BatteryCount;
    public int MaximumBattery;
    [Space(5)]

    [Header("Lite Flashlight - Attributes")]
    [Space]
    public float LF_Intensity;
    public float LF_Range;
    public float LF_SpotAngle;
    public float LF_DecreaseSpeed;
    public Light LF_LightSource;
    [Space(5)]

    [Header("Jumbo Flashlight - Attributes")]
    [Space]
    public float JF_Intensity;
    public float JF_Range;
    public float JF_SpotAngle;
    public float JF_DecreaseSpeed;
    public Light JF_LightSource;
    [Space(5)]

    [Header("Sound Attributes")]
    [Space]
    public AudioClip EnableSound;
    public AudioClip DisableSound;
    public AudioClip BatteryAddSound;
    public AudioClip NoBatterySound;
    public AudioSource _AudioSource;
    [Space(5)]

    [Header("UI Attributes")]
    [Space]
    public Text BatteryCountText;
    public GameObject _20Percent;
    public GameObject _40Percent;
    public GameObject _60Percent;
    public GameObject _80Percent;
    public GameObject _100Percent;
    public GameObject UIParent;

    bool ParentDraw= true;
    bool BatteryDraw = true;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (LiteFlashlight)
        {
            if (Health > 0.0f)
            {
                Health -= Time.deltaTime * LF_DecreaseSpeed;
            }

            if (Health < 25)
            {
                float pi = Time.time / 0.2f * 2 * Mathf.PI;
                float amplitude = Mathf.Cos(pi) * 0.5f + LF_Intensity;
                LF_LightSource.GetComponent<Light>().intensity = amplitude + Random.Range(0.1f, 1);
            }
            //LF_LightSource.GetComponent<Light>().color = new Color(Health / 100, Health / 100, Health / 100, Health / 100);
        }

        if (JumboFlashlight)
        {
            if (Health > 0.0f)
            {
                Health -= Time.deltaTime * JF_DecreaseSpeed;
            }

            if (Health < 25)
            {
                float pi = Time.time / 0.1f * 1 * Mathf.PI;
                float amplitude = Mathf.Cos(pi) * 0.5f + JF_Intensity;
                JF_LightSource.GetComponent<Light>().intensity = amplitude + Random.Range(0.1f, 1);
            }
            //JF_LightSource.GetComponent<Light>().color = new Color(Health / 100, Health / 100, Health / 100, Health / 100);
        }

        BatteryCountText.text = BatteryCount + "/5";

        if (!ParentDraw)
            UIParent.GetComponent<CanvasGroup>().alpha += Time.deltaTime;
        else
            UIParent.GetComponent<CanvasGroup>().alpha -= Time.deltaTime;

        if (!BatteryDraw)
            BatteryCountText.GetComponent<CanvasGroup>().alpha += Time.deltaTime;
        else
            BatteryCountText.GetComponent<CanvasGroup>().alpha -= Time.deltaTime;




        InputFunctionality();
        Recharge();
        HealthUI();
    }

    void InputFunctionality()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (hasLF)
            {
                if (LiteFlashlight)
                {
                    LF_Go.SetActive(false);
                    LiteFlashlight = false;
                    JumboFlashlight = false;
                    _AudioSource.PlayOneShot(DisableSound);
                    ParentDraw = true;
                }
                else
                {
                    LF_Go.SetActive(true);
                    LiteFlashlight = true;
                    JumboFlashlight = false;
                    LF_Functionality();
                    ParentDraw = false;
                    StartCoroutine(BatteryUIFadeOut());
                }

            }
            else
            {
                UIManager.instance.DisplayIN("Not Have a Flashlight", 2f);
            }
            if (hasJF)
            {
                if (JumboFlashlight)
                {
                    JF_Go.SetActive(false);
                    JumboFlashlight = false;
                    LiteFlashlight = false;
                    _AudioSource.PlayOneShot(DisableSound);
                    ParentDraw = true;
                }
                else
                {
                    JF_Go.SetActive(true);
                    JumboFlashlight = true;
                    LiteFlashlight = false;
                    JF_Functionality();
                    ParentDraw = false;
                    StartCoroutine(BatteryUIFadeOut());
                }
            }
            else
            {
                UIManager.instance.DisplayIN("Not Have a Flashlight", 2f);
            }
        }
    }

    void Recharge()
    {
        if (BatteryCount >= 1 && Health <= 80)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                BatteryCount -= 1;
                Health += 20;
                _AudioSource.PlayOneShot(BatteryAddSound);
                StartCoroutine(BatteryUIFadeOut());
            }
        }
        if (BatteryCount == 0)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                UIManager.instance.DisplayIN("Not Have Enough Battery", 2f);
                _AudioSource.PlayOneShot(NoBatterySound);
                StartCoroutine(BatteryUIFadeOut());
            }
        }
    }

    void HealthUI()
    {
        if (Health == 0)
        {
            _20Percent.SetActive(false);
            _40Percent.SetActive(false);
            _60Percent.SetActive(false);
            _80Percent.SetActive(false);
            _100Percent.SetActive(false);
        }
        else if (Health <= 20)
        {
            _20Percent.SetActive(true);
            _40Percent.SetActive(false);
            _60Percent.SetActive(false);
            _80Percent.SetActive(false);
            _100Percent.SetActive(false);

        }
        else if (Health <= 40 && Health > 20)
        {
            _20Percent.SetActive(true);
            _40Percent.SetActive(true);
            _60Percent.SetActive(false);
            _80Percent.SetActive(false);
            _100Percent.SetActive(false);
        }
        else if (Health <= 60 && Health > 40)
        {
            _20Percent.SetActive(true);
            _40Percent.SetActive(true);
            _60Percent.SetActive(true);
            _80Percent.SetActive(false);
            _100Percent.SetActive(false);
        }
        else if (Health <= 80 && Health > 60)
        {
            _20Percent.SetActive(true);
            _40Percent.SetActive(true);
            _60Percent.SetActive(true);
            _80Percent.SetActive(true);
            _100Percent.SetActive(false);
        }
        else if (Health <= 100 && Health > 80)
        {
            _20Percent.SetActive(true);
            _40Percent.SetActive(true);
            _60Percent.SetActive(true);
            _80Percent.SetActive(true);
            _100Percent.SetActive(true);
        }
    }

    void LF_Functionality()
    {
        LF_LightSource.intensity = LF_Intensity;
        LF_LightSource.range = LF_Range;
        LF_LightSource.spotAngle = LF_SpotAngle;
        _AudioSource.PlayOneShot(EnableSound);
    }

    void JF_Functionality()
    {
        JF_LightSource.intensity = JF_Intensity;
        JF_LightSource.range = JF_Range;
        JF_LightSource.spotAngle = JF_SpotAngle;
        _AudioSource.PlayOneShot(EnableSound);
    }

    IEnumerator BatteryUIFadeOut()
    {
        BatteryDraw = false;
        yield return new WaitForSeconds(2f);
        BatteryDraw = true;
    }


}
